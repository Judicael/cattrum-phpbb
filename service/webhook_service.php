<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\service;

/**
 * @ignore
 */
use Symfony\Component\HttpFoundation\JsonResponse;
use OAuth\Common\Http\Client\StreamClient;
use OAuth\Common\Http\Client\CurlClient;
use OAuth\Common\Http\Uri\Uri;
// use gaki\cattrum\service\db_service;
use gaki\cattrum\service\discord_message_builder;

/**
 * cattrum webhook service.
 */
class webhook_service
{

	/** @var \phpbb\user */
	private $user;

	/** @var \phpbb\config\config */
	private $config;	

	/** @var \phpbb\language\language */
	private $language;	

	/**
	 * Constructor
	 */
	public function __construct(
		\phpbb\config\config $config,
		\phpbb\user $user,
		\phpbb\language\language $language,
		\phpbb\feed\helper $feed_helper,
		$phpbb_root_path
        ) 
	{
		$this->config				= $config;
		$this->user 				= $user;
		$this->language				= $language;
		$this->feed_helper			= $feed_helper;
		$this->phpbb_root_path		= $phpbb_root_path;
    }
    
    /**
     *@return \OAuth\Common\Http\Client\ClientInterface  ClientInterface
     */
	private function getHttpClient() 
	{
		//return new StreamClient();
		return new CurlClient();
	} 
	
	public function newPost($whUrls, $post_data) 
	{
		$username = $post_data['user']['username'];
		$avatar_url = $post_data['user']['avatar'];
		$content = $post_data['content'];
		$topic_title = $this->language->lang('CATTRUM_DISCORD_NEW_POST', $post_data['topic_title']);
		$uid = $post_data['bbcode_uid'];
		$bitfield = $post_data['bbcode_bitfield'];
		$url = $post_data['url_post'];
		$this->sendWebHook($whUrls, $username, $avatar_url, $topic_title, $content, $uid, $bitfield, $url);
	}

	public function updatePost($whUrls, $post_data) 
	{
		$username = $post_data['edit_user']['username'];
		$avatar_url = $post_data['edit_user']['avatar'];
		$content = $post_data['content'];
		$topic_title = $this->language->lang('CATTRUM_DISCORD_UPDATE_POST', $post_data['topic_title']);
		$uid = $post_data['bbcode_uid'];
		$bitfield = $post_data['bbcode_bitfield'];		
		$url = $post_data['url_post'];		
		$this->sendWebHook($whUrls, $username, $avatar_url, $topic_title, $content, $uid, $bitfield, $url);
	} 	

	public function newTopic($whUrls, $post_data) 
	{
		$username = $post_data['user']['username'];
		$avatar_url = $post_data['user']['avatar'];
		$content = $post_data['content'];
		$topic_title = $this->language->lang('CATTRUM_DISCORD_NEW_TOPIC', $post_data['topic_title']);;
		$uid = $post_data['bbcode_uid'];
		$bitfield = $post_data['bbcode_bitfield'];		
		$url = $post_data['url_topic'];		
		$this->sendWebHook($whUrls, $username, $avatar_url, $topic_title, $content, $uid, $bitfield, $url);
	}

	public function updateTopic($whUrls, $post_data) 
	{
		$username = $post_data['edit_user']['username'];
		$avatar_url = $post_data['edit_user']['avatar'];
		$content = $post_data['content'];
		$topic_title = $this->language->lang('CATTRUM_DISCORD_UPDATE_TOPIC', $post_data['topic_title']);
		$uid = $post_data['bbcode_uid'];
		$bitfield = $post_data['bbcode_bitfield'];		
		$url = $post_data['url_topic'];	
		$this->sendWebHook($whUrls, $username, $avatar_url, $topic_title, $content, $uid, $bitfield, $url);
	}

	private function sendWebHook($whUrls, $username, $avatar_url, $topic_title, $content, $uid, $bitfield, $url) 
	{
		// We do not use specify embeds as it's limited
		// We prefers use the automatic embeds link generation.

		// Convert DB content storage format to HTML
		$options = ($bitfield ? OPTION_FLAG_BBCODE : 0);
		$content = $this->feed_helper->generate_content($content, $uid, $bitfield, $options, null, array());
		
		// Convert HTML to discord markdown
		$result = $this->convertPostToMarkdown($content);

		$mb = new discord_message_builder();
		$mb
			->withTitle($topic_title, $url)
			->withoutQuote()
			->withTitleIcon('')
			->withContent($result['markdown']);

		foreach ($result['links'] as $link) {
			$mb->addFakeEmbed($link);
		}

		$requestBody = array(
			'username'		=> $username,
			'avatar_url'	=> $avatar_url,
			'content'		=> $mb->build(),
		);

		$whUrlsList  = preg_split('/[, ;\n\r]+/', $whUrls, -1, PREG_SPLIT_NO_EMPTY);
		foreach ($whUrlsList as $whUrl) {
			$this->getHttpClient()->retrieveResponse(
				new Uri($whUrl), 
				json_encode($requestBody, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), 
				array('Content-Type' => 'application/json'), 
				'POST'
			);
		}
	}

	private function convertPostToMarkdown($content) 
	{		
		$regexBlankLine = "/((?m)^[ \t]*\r?\n)+/m";
		$regexFindLink  = "/\"((?:https*\\:){0,1}(\\/\\/[^\\\"]*))\"/";

		// Load the XML source
		// We normalize manualy instead of ->normalizeDocument() for being able to change the html namespace 
		// to XHTML namespace overwise the XSLT will not match
		$xml = new \DOMDocument('1.0', 'UTF-8');
		// Load dirty HTML
		$xml->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8')); 	
		// Force to produce valid XML
		$content = $xml->saveXML();
		// Change the namespace to XHTML one overwise the XSLT will not match
		$content = str_replace('<html>', '<html xmlns="http://www.w3.org/1999/xhtml">', $content);
		// Re-Import with valid XML + XHTML namespace
		$xml->loadXML($content); 
	
		$xsl = new \DOMDocument('1.0', 'UTF-8');
		$xsl->load($this->phpbb_root_path . '/ext/gaki/cattrum/service/markdown.xslt');

		// Configure the transformer
		$proc = new \XSLTProcessor;
		$proc->importStyleSheet($xsl); // attach the xsl rules

		// Test with https://www.online-toolz.com/tools/xslt-validator-tester-online.php
		$result = $proc->transformToXML($xml);

		// Clean multiple blank line
		$result = preg_replace($regexBlankLine, "\n", $result);

		// Extract all links
		$links = array();
		preg_match_all($regexFindLink, $content, $links, PREG_PATTERN_ORDER);
		$links = is_array($links) && count($links) > 1 ? array_unique($links[1]) : array();

		// Clean links by remove smilies and w3.org links
		$linksClean = array();
		$smiliesPath = $this->config['smilies_path'];
		foreach ($links as $link) {
			if (strpos($link, $smiliesPath) === false 
					&& strpos($link, 'www.w3.org') === false
					&& strpos($link, 'twemoji.maxcdn.com') === false) {
				$linksClean[] = $link;
			}
		}

		return array(
			'markdown' => $result,
			'links' => $linksClean,
		);
	} 
}
