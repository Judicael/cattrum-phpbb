<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\service;

/**
 * @ignore
 */
use gaki\cattrum\service\db_service;

/**
 * cattrum user service.
 */
class user_service
{

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\config\config */
	protected $config;	

	/** @var \phpbb\auth\provider\oauth\oauth */
	protected $oauth;

	/** @var \phpbb\files\factory */
	protected $files_factory;

	/** @var \phpbb\language\language */
	protected $language;	

	/** @var \gaki\cattrum\service\db_service */
	protected $db_service;	

    /** @var phpbb\passwords\manager */
	protected $passwords_manager;
	
    /** @var string */
	protected $phpbb_root_path;	
	
    /** @var string */
    protected $php_ext;

	/**
	 * Constructor
	 */
	public function __construct(
		\phpbb\config\config $config,
		\phpbb\passwords\manager $passwords_manager,
		\phpbb\user $user,
		\phpbb\auth\provider\oauth\oauth $oauth,
		\phpbb\files\factory $files_factory,
		\phpbb\language\language $language,
		db_service $db_service,
		$phpbb_root_path,
		$php_ext) 
	{
		$this->config				= $config;
		$this->passwords_manager	= $passwords_manager;
		$this->user 				= $user;
		$this->oauth				= $oauth;
		$this->files_factory		= $files_factory;
		$this->language				= $language;
		$this->db_service			= $db_service;
		$this->phpbb_root_path		= $phpbb_root_path;
		$this->php_ext				= $php_ext;
	}

	/**
	 * Create a new user in database
	 */
	public function create_user($user_info, $providerName)
	{
		$user_row = array();

        // User functions
        if (!function_exists('user_add')) {
            require $this->phpbb_root_path . 'includes/functions_user.' . $this->php_ext;
        }

        // String functions
        if (!function_exists('gen_rand_string')) {
            require $this->phpbb_root_path . 'includes/functions.' . $this->php_ext;
		}
		
        // Detect the default language of the forum.
        if (!empty($this->config['default_lang'])) {
            $user_row['user_lang'] = trim($this->config['default_lang']);
        } else {
			// Use english
            $user_row['user_lang'] = 'en';
        }		

		// Default group_id is required.		
		$group_id = $this->db_service->get_default_group_id();
        if (!is_numeric($group_id)) {
			// No group has been set.
			trigger_error('NO_GROUP');
		} else {
			$user_row['group_id'] = $group_id;
		} 
		
        // Activation Required.
		if (!$user_info['verified'] 
				&& ($this->config['require_activation'] == USER_ACTIVATION_SELF || $this->config['require_activation'] == USER_ACTIVATION_ADMIN) 
				&& $this->config['email_enable']) {
            $user_row['user_type'] = USER_INACTIVE;
            $user_row['user_actkey'] = gen_rand_string(mt_rand(6, 10));

            $user_row['user_inactive_reason'] = INACTIVE_REGISTER;
            $user_row['user_inactive_time'] = time();
        }
        // No Activation Required.
        else
        {
            $user_row['user_type'] = USER_NORMAL;
            $user_row['user_actkey'] = '';

            $user_row['user_inactive_reason'] = 0;
            $user_row['user_inactive_time'] = 0;
		}

		// On 3.3 max_pass_chars is empty by default
		$min_pass_chars = (int) $this->config['min_pass_chars'];
		$max_pass_chars = (int) $this->config['max_pass_chars'];
		$max_pass_chars = $max_pass_chars < $min_pass_chars ? $min_pass_chars : $max_pass_chars;

		// Generate a random password.
		$new_password = gen_rand_string_friendly(max(8, mt_rand($min_pass_chars, $max_pass_chars)));
		$user_row['user_password'] = $this->passwords_manager->hash($new_password);
		
		// Fill ip, name, email, lastvisit
		$username = preg_replace( '/[^[:print:]]/', '', $user_info['username']);
		$user_row['user_ip'] = $this->user->ip;
		$user_row['user_lastvisit'] = time();
		$user_row['username'] = $username;
		$user_row['user_email'] = $user_info['email'];
		
        // Adds the user to the Newly registered users group.
        if ($this->config['new_member_post_limit'])
        {
            $user_row['user_new'] = 1;
		}
		
		$user_row[db_service::TABLE_USER_CATTRUM] = 1;

		// Test if the unique constrain on username_clean column is repected
		if ($this->db_service->username_clean_exist($user_row['username'])) {
			trigger_error($this->language->lang('USERNAME_ALREADY_PRESENT', $user_row['username']));
		} 

		// Register user, with optional custom fields.
		$cp_data = array();
        $user_id = user_add($user_row, $cp_data);

		// This should not happen, because the required variables are listed above.
        if ($user_id === false)
        {
            trigger_error('NO_USER', E_USER_ERROR);
        }
        // User added successfully.
        else
        {
			// Link the account
			$link_data = $this->get_login_link_data_array();
			$link_data['user_id'] = $user_id;
			$link_data['link_method'] = 'login_link'; 
			$link_data['oauth_service'] = $providerName;
			$this->oauth->link_account($link_data);

			// Upload the avatar: inspired by \phpbb\avatar\driver\upload  
			if (!empty($user_info['avatar'])) {
				/** @var \phpbb\files\upload $upload */
				$upload = $this->files_factory->get('upload')
					->set_error_prefix('AVATAR_')
					->set_allowed_extensions(array(
						'gif',
						'jpg',
						'jpeg',
						'png',
					))
					->set_max_filesize($this->config['avatar_filesize'])
					->set_allowed_dimensions(
						$this->config['avatar_min_width'],
						$this->config['avatar_min_height'],
						$this->config['avatar_max_width'],
						$this->config['avatar_max_height'])
					->set_disallowed_content((isset($this->config['mime_triggers']) ? explode('|', $this->config['mime_triggers']) : false));

				$file = $upload->handle_upload('files.types.remote', $user_info['avatar']);
				if (count($file->error))
				{
					// Avatar do not match forum rules
					$file->remove();
					// Try with a random avatar
					$file = $upload->handle_upload('files.types.remote', 'https://robohash.org/'. $user_id .'.png?size=60x60&set=set4&regex-fake.png');
				}

				$prefix = $this->config['avatar_salt'] . '_';
				$file->clean_filename('avatar', $prefix, $user_id);

				// If there was an no error during upload, then abort operation
				if (count($file->error) === 0)
				{
					// Calculate new destination
					$destination = $this->config['avatar_path'];

					// Adjust destination path (no trailing slash)
					if (substr($destination, -1, 1) == '/' || substr($destination, -1, 1) == '\\')
					{
						$destination = substr($destination, 0, -1);
					}

					$destination = str_replace(array('../', '..\\', './', '.\\'), '', $destination);
					if ($destination && ($destination[0] == '/' || $destination[0] == "\\"))
					{
						$destination = '';
					}
				
					// Move file and overwrite any existing image
					$file->move_file($destination, true);
					if (count($file->error))
					{
						$file->remove();
						return false;
					}

					// Update user
					$this->db_service->set_or_update_user_avatar($user_id, $file->get('extension'), $file->get('width'), $file->get('height'));
					
				} else {
					// Skip the set of avatar and continue			 
				} 
			} 

			// Finish login
			$this->user->session_create($user_id, false, false, true);

			$this->perform_redirect();
		}
	}  

	/**
	* Performs a post login redirect
	*/
	private function perform_redirect()
	{
		$url = append_sid($this->phpbb_root_path . 'index.' . $this->php_ext);
		redirect($url);
	}

	/**
	* Builds the login_link data array: Use for associate the external account
	*
	* @return	array	All login_link data. This is all GET data whose names
	*					begin with 'login_link_'
	*/
	private function get_login_link_data_array()
	{
		global $request;

		$var_names = $request->variable_names(\phpbb\request\request_interface::GET);
		$login_link_data = array();
		$string_start_length = strlen('login_link_');

		foreach ($var_names as $var_name)
		{
			if (strpos($var_name, 'login_link_') === 0)
			{
				$key_name = substr($var_name, $string_start_length);
				$login_link_data[$key_name] = $request->variable($var_name, '', false, \phpbb\request\request_interface::GET);
			}
		}

		return $login_link_data;
	}

	/**
	 * For new topic $forum_id is needed ($topic_id set to 0)
	 * For edit $topic_id is needed and ($forum_id set to 0) 
	 */
	public function submit_post($username, $subject, $text, $forum_id=0, $topic_id = 0) {
		if (!function_exists('submit_post')) {
            require $this->phpbb_root_path . 'includes/functions_posting.' . $this->php_ext;
		}

		if (!function_exists('censor_text')) {
            require $this->phpbb_root_path . 'includes/functions_content.' . $this->php_ext;
		}

		$subject = censor_text(utf8_normalize_nfc($subject));
		$text    = censor_text(utf8_normalize_nfc($text));

		// Is a new topic or an update topic
		$mode = $topic_id === 0 ? 'post' : 'edit';

		// If in edition we need some additional informations and flags
		$dataUpdate = array();
		if ($mode === 'edit') {
			$topic_info = $this->db_service->get_topic_info($topic_id);
			$dataUpdate = array(
				// Folowing is needed for update the topic
				'topic_id'				=> $topic_id,
				'post_id'				=> $topic_info['topic_first_post_id'],
				'topic_first_post_id'	=> $topic_info['topic_first_post_id'],
				'poster_id'				=> ANONYMOUS,
			);
			// We make sure it's the forum which hold the topic
			$forum_id = $topic_info['forum_id'];
		} 
		
		// Generate $text, $bbcode_uid, $bbcode_bitfield and $flags values
		generate_text_for_storage($text, $bbcode_uid, $bbcode_bitfield, $flags, true, true, true, true, true, true, true, $mode);
		$data = array(
			'forum_id'			=> $forum_id,
			'icon_id'			=> false,

			// New topic information
			'post_time'			=> time(),
			'topic_title'		=> $subject,
			'message_md5'		=> (string) md5($text),						
			'message'			=> $text,
			'bbcode_bitfield'	=> $bbcode_bitfield, 
			'bbcode_uid'		=> $bbcode_uid,
			'poster_ip'			=> $user->ip,			
			'post_edit_locked'	=> 1,
			'enable_sig'		=> true,
			'enable_bbcode'		=> true,
			'enable_smilies'	=> true,
			'enable_urls'		=> true,
			'notify_set'		=> false,
			'notify'			=> false,
			'enable_indexing'	=> true,

			// Enable capacity to topic edit (see submit_post() for have $post_mode = 'edit_topic' )
			'topic_posts_approved'	=> 1, 

			// 3.0.6
			'force_approved_state'	=> true,
			// 3.1.0
			'force_visibility'      => true,
		);
		
		$poll = null;
		$post_data = array_merge($data, $dataUpdate);
		$relative_url = submit_post ( $mode,  $subject, $username,  POST_NORMAL, $poll,  $post_data);

		// Convert to absolute url
		$relative_url = str_replace('../','', $relative_url);
		$relative_url = str_replace('./','', $relative_url);
		
		return array (
			'url' => generate_board_url() . '/' . htmlspecialchars_decode($relative_url),
			'forum_id' => $post_data['forum_id'],
			'topic_id' => $post_data['topic_id'],
			'post_id' => $post_data['post_id'],
		);
	} 
}
