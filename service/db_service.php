<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\service;

/**
 * cattrum Service info.
 */
class db_service
{
	/**
	 * Collumn name of table cattrum_values
	 */
	public const TABLE_USER_CATTRUM	= 'USER_CATTRUM';

	/**
	 * Collumn name of table cattrum_values
	 */
	public const TABLE_VALUES_ID	= 'cat_values_id';
	public const TABLE_VALUES_TYPE	= 'cat_values_type';
	public const TABLE_VALUES_KEY	= 'cat_values_key';
	public const TABLE_VALUES_DATE	= 'cat_values_date';
	public const TABLE_VALUES_VALUE = 'cat_values_value';	
	
	/**
	 * Table cattrum_values values of column cattrum_type
	 */
	public const TYPE_AUTH_TEMP = 'TYPE_AUTH_TEMP';
	public const TYPE_DISCORD_NOTIFICATION_FORUM = 'TYPE_DISCORD_NOTIFICATION_FORUM';
	public const TYPE_CANDIDATE_TOPIC = 'TYPE_CANDIDATE_TOPIC';
	public const TYPE_CANDIDATE_CONTENT = 'TYPE_CANDIDATE_CONTENT';
	public const TYPE_CANDIDATE_TEMPLATE = 'TYPE_CANDIDATE_TEMPLATE';


	/** @var \phpbb\user */
	protected $user;

    /** @var \phpbb\db\driver\driver_interface */
    protected $db;	

	/** 
	 * The name of cattrum values table in database.
	 * 
	 * @var string 
	 */
	protected $values_table_name;

    /** @var string */
    protected $table_prefix;	

	/**
	 * Constructor
	 */
	public function __construct(
		\phpbb\user $user, 
		\phpbb\db\driver\driver_interface $db, 
		$values_table_name,
		$table_prefix)
	{
		$this->user = $user;
		$this->db = $db;
		$this->values_table_name = $values_table_name;
		$this->table_prefix = $table_prefix;
	}

	/**
	 * Get user object
	 *
	 * @return \phpbb\user $user User object
	 */
	public function get_session_id()
	{
		return $this->user->session_id;
	}

	public function set_or_update_values($type, $key, $value, $date = null)
	{
		$this->delete_values($type, $key);
		return $this->set_values($type, $key, $value, $date);
	}

	public function set_or_update_user_avatar($user_id, $file_ext, $width, $height)
	{
		$sql_arr = array();
		$sql_arr['user_avatar'] = ($user_id . '_' . time() . '.' . $file_ext);
		$sql_arr['user_avatar_type'] = AVATAR_UPLOAD;
		$sql_arr['user_avatar_width'] = $width;
		$sql_arr['user_avatar_height'] = $height;

		// Update user
		$sql = 'UPDATE ' . USERS_TABLE . ' SET ' . $this->db->sql_build_array('UPDATE', $sql_arr) . ' WHERE user_id = ' . (int) $user_id;
		$this->db->sql_query($sql);	
	} 

	public function username_clean_exist($username) 
	{
		// The only constrain in USER table is on the username_clean column
		$username_clean = utf8_clean_string($username);
		$sql = "SELECT count(*) as 'exist' FROM " . USERS_TABLE . " WHERE username_clean = '$username_clean'";
		$result = $this->db->sql_query($sql);
		return $this->db->sql_fetchfield('exist') !== "0";
	} 

	public function set_values($type, $key, $value, $date = null)
	{
		if ($date === null) {
			$date = time();
		} 

		$sql = 'INSERT INTO ' . $this->values_table_name . ' ' . $this->db->sql_build_array('INSERT', array(
			db_service::TABLE_VALUES_TYPE		=> (string) $type,
			db_service::TABLE_VALUES_KEY		=> (string) $key,
			db_service::TABLE_VALUES_DATE		=> (int) $date,
			db_service::TABLE_VALUES_VALUE		=> (string) json_encode($value, JSON_UNESCAPED_SLASHES), // We need to escape all unicode char for DB compatibility
		));
		
		$result = $this->db->sql_query($sql);
		$this->db->sql_freeresult($result);

		return $result;
	}

	/**
	 * Return all values of a specific type and optionnaly with a specific key value.
	 */
	public function get_values($type, $key=null, $date=null, $before_date=null, $after_date=null)
	{		
		$sql_array = array(
			'SELECT'	=> 'v.*',
			'FROM'		=> array($this->values_table_name  => 'v'),
			'WHERE'		=> 'v.' . db_service::TABLE_VALUES_TYPE . " = '$type'",
		);
		if ($key !== null) {
			$sql_array['WHERE'] .= ' AND v.' . db_service::TABLE_VALUES_KEY . " = '$key'";
		}
		if ($before_date !== null) {
			$sql_array['WHERE'] .= ' AND (v.' . db_service::TABLE_VALUES_DATE . " < '$before_date'";
			if ($date !== null) {
				$sql_array['WHERE'] .= ' OR v.' . db_service::TABLE_VALUES_DATE . " = '$date'";
			}
			$sql_array['WHERE'] .= ')';
		}
		if ($after_date !== null) {
			$sql_array['WHERE'] .= ' AND (v.' . db_service::TABLE_VALUES_DATE . " > '$after_date'";
			if ($date !== null) {
				$sql_array['WHERE'] .= ' OR v.' . db_service::TABLE_VALUES_DATE . " = '$date'";
			} 
			$sql_array['WHERE'] .= ')';
		}
		if ($date !== null && $before_date == null && $after_date == null ) {
			$sql_array['WHERE'] .= ' AND v.' . db_service::TABLE_VALUES_DATE . " = '$date'";
		}

		$sql = $this->db->sql_build_query('SELECT', $sql_array);
		$result = $this->db->sql_query($sql);

		$values = array();
		while ($row = $this->db->sql_fetchrow($result)) {
			$row[db_service::TABLE_VALUES_VALUE] = json_decode($row[db_service::TABLE_VALUES_VALUE], true);
			array_push($values, $row);
		}
		$this->db->sql_freeresult($result);

		return $values;
	}

	public function delete_values($type, $key=null, $date=null, $before_date=null, $after_date=null, $db_id=null)
	{	
		$sql = 'DELETE FROM ' . $this->values_table_name . ' WHERE ' . db_service::TABLE_VALUES_TYPE . "='$type'";
		if ($db_id !== null) {
			$sql .= ' AND ' . db_service::TABLE_VALUES_ID . " = '$db_id'";
		}		
		if ($key !== null) {
			$sql .= ' AND ' . db_service::TABLE_VALUES_KEY . " = '$key'";
		}
		if ($before_date !== null) {
			$sql .= ' AND (' . db_service::TABLE_VALUES_DATE . " < '$before_date'";
			if ($date !== null) {
				$sql .= ' OR ' . db_service::TABLE_VALUES_DATE . " = '$date'";
			}
			$sql .= ')';
		}
		if ($after_date !== null) {
			$sql .= ' AND (' . db_service::TABLE_VALUES_DATE . " > '$after_date'";
			if ($date !== null) {
				$sql .= ' OR ' . db_service::TABLE_VALUES_DATE . " = '$date'";
			} 
			$sql .= ')';
		}
		if ($date !== null && $before_date == null && $after_date == null ) {
			$sql .= ' AND ' . db_service::TABLE_VALUES_DATE . " = '$date'";
		}			
		$result = $this->db->sql_query($sql);

		return $result;
	}

    /**
     * Get the default group_id for new users
     */
    public function get_default_group_id()
    {
        // Read the default group.
        $sql = "SELECT group_id FROM " . GROUPS_TABLE . " WHERE group_name = 'REGISTERED' AND group_type = " . GROUP_SPECIAL;
        $query = $this->db->sql_query($sql);
        $result = $this->db->sql_fetchrow($query);
        $this->db->sql_freeresult($query);

        // Group found;
        if (is_array($result) && isset($result['group_id'])) {
            return $result['group_id'];
        }

        // Not found

        return false;
	}
	
	/**
	 * Retrieves the name of a user from the database when given an ID
	 * @param $user_id The ID of the user to query
	 * @return The name of the user, or NULL if not found
	 */
	public function query_user($user_id)
	{
		// Avoid inject SQL
		if (is_numeric($user_id) == false) {
			return null;
		}

		$sql = "SELECT username, user_avatar from " . USERS_TABLE . " WHERE user_id = $user_id";
		$result = $this->db->sql_query($sql);
		$data = $this->db->sql_fetchrow($result);
		$name['username']	= $data['username'];
		$name['avatar']		= $data['user_avatar'];
		$this->db->sql_freeresult($result);
		return $name;
	}
	
	public function get_topic_info($topic_id) 
	{
		// Avoid inject SQL
		if (is_numeric($topic_id) == false) {
			return null;
		}

		$sql = "SELECT topic_first_post_id, forum_id from " . TOPICS_TABLE . " WHERE topic_id = $topic_id";
		$result = $this->db->sql_query($sql);
		$data = $this->db->sql_fetchrow($result);
		$topic_first_post_id = (int) $data['topic_first_post_id'];
		$forum_id = (int) $data['forum_id'];
		$this->db->sql_freeresult($result);
		return array(
			'topic_first_post_id' => $topic_first_post_id,
			'forum_id' => $forum_id,
		);
	} 
		
}
