<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\service;

class discord_message_builder {

    private const MAX_LENGTH = 1900;

	private $sender;

	private $title;

	private $titleUrl;

	private $titleIcon = "✎ ";
	
	private $content;

	private $fakeEmbedContent = "";

	private $withQuote = ">>> ";

	private $titleAuthor;

	/**
	 * Constructor
	 */
	public function __construct() 
	{
    }
    
    /** @return gaki\cattrum\service\MessageBuilder */
	public function withContent($content, $maxLength=null) { 
		if ($content != null) {
			$this->content = $this->cutText($content, $maxLength);
		}
		return $this;
	}

    /** @return gaki\cattrum\service\MessageBuilder */
	public function withSender(string $sender) {
		$this->sender = $sender;
		return $this;
	}

    /** @return gaki\cattrum\service\MessageBuilder */
	public function withTitle(string $title, $titleUrl=null, $titleAuthor=null) {
		$this->title = $title;
		$this->titleUrl = $titleUrl;
		$this->titleAuthor = $titleAuthor;
		return $this;
	}

    /** @return gaki\cattrum\service\MessageBuilder */
	public function addFakeEmbed($url, $legend=null) {
		if (!empty($this->fakeEmbedContent)) {
			$this->fakeEmbedContent .= "\n";
		}
		$this->fakeEmbedContent .= "∞ ";
		if ($legend != null) {
			$this->fakeEmbedContent .= "***" . legend . "*** ";
		}
		if ($url != null) {
			$this->fakeEmbedContent .= $url;
		}
		return $this;
	}
   
    /** @return string */
	private function cutText(string $text, $maxLength=null) {
		$result = '';
		$cut = $maxLength;
		if ($cut > $this::MAX_LENGTH || $cut == null ) {
			$cut = $this::MAX_LENGTH - 500;
		}
		if (strlen($text) > $cut) {
			$result = substr($text, 0, $cut - 1 - 2) . '……';
		} else {
			$result = $text;
		}
		return $result;
	}

    /** @return gaki\cattrum\service\MessageBuilder */
	public function withoutQuote() {
		$this->withQuote = "";
		return $this;
	}

	/** @return gaki\cattrum\service\MessageBuilder */
	public function withTitleIcon($titleIcon){
		$this->titleIcon = $titleIcon;
		return $this;
	} 

    /** @return string */
	public function build() {
		$content = $this->withQuote;

		// Add title
		if ($this->title != null) {
			$contentAuthors = "";
			if ($this->titleAuthor != null && !empty($this->titleAuthor)) {
				$contentAuthors = "  " . $this->titleAuthor;
			}
			$content .= $this->titleIcon . "***" . $this->title . "***" . $contentAuthors . "\n";
			// Add title url
			if ($this->titleUrl != null) {
				$content .= "∞ " . $this->titleUrl . "\n" ;
			} 
			$content .= "\n";
		}

		// Add Sender and content
		if ($this->sender != null || $this->content != null || $this->fakeEmbedContent != null) {
			if ($this->sender != null) {
				$content .= "***" . $this->sender . "*** 📟   \n";
			}
			if ($this->content != null) {
				$content .= $this->content;
			}
			if (!empty($this->fakeEmbedContent)) {
				if (!empty($content) && $this->withQuote != $content) {
					$content .= "\n";
				}
				$content .= $this->fakeEmbedContent;
			}
			$content = $this->cutText($content, $this::MAX_LENGTH);
        }
        
		return $content;
	}

} 