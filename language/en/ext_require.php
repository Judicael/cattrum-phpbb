<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

/**
* Some characters you may want to copy&paste: ’ » “ ” …
*/

$lang = array_merge($lang, [
	'ERROR_PHPBB_VERSION'	=> 'Minimum phpBB version required is <b>%1$s</b> but less than %2$s',
	'ERROR_PHP_CURL'		=> 'Need to have <b>php-curl</b> installed (sudo apt-get install php-curl)',
	'ERROR_PHP_XSL'			=> 'Need to have <b>php-xls</b> installed (sudo apt-get install php-xsl)',
]);
