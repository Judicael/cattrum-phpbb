<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, [
	'ACP_CATTRUM_TITLE'						=> 'Cattrum Module',
	'ACP_CATTRUM'							=> 'Cattrum Settings',

	'ACP_CATTRUM_ADD_NAVBAR'				=> 'Add login icon in nav bar',
	'ACP_CATTRUM_DISCORD_BOT_ALLOWED'		=> 'Discord bot allowed (coma or space separated)',
	'ACP_CATTRUM_DISCORD_BOT_FORUM'			=> 'Discord bot forum',
	'ACP_CATTRUM_CANDIDATE_TEMPLATE'		=> 'Post template for candidate',
	'ACP_CATTRUM_SETTING_SAVED'				=> 'Settings have been saved successfully!',	
	'SETTINGS_DISCORD_WEBHOOK'				=> 'Discord Webhooks',
	'SETTINGS_GENERAL'						=> 'General',
	'ACP_CATTRUM_FORUM'						=> 'Select Forums (multiple allowed)',
	'ACP_CATTRUM_NOTIFICATION_FOR'			=> 'Send notifications for',
	'ACP_CATTRUM_NOTIFICATION_NEW_TOPIC'	=> 'New Topic',
	'ACP_CATTRUM_NOTIFICATION_NEW_POST'		=> 'New Post',
	'ACP_CATTRUM_NOTIFICATION_UPDATE_TOPIC'	=> 'Update Topic',
	'ACP_CATTRUM_NOTIFICATION_UPDATE_POST'	=> 'Update Post',
	'ACP_CATTRUM_WH_URLS'					=> 'URL Discord Webhooks (cariage return, coma or space separated)',
	'ACP_CATTRUM_WH_TOPICS'					=> 'Only for topics ids (cariage return, coma or space separated)',
	'ACP_CATTRUM_WH_USERS'					=> 'Only for usernames (cariage return, coma or space separated)',

	'ACP_CATTRUM_NOTE_FOUND_TOPIC_ID'			=> 'NOTE: Topic ID is a number and can be found in forum URL at the parameter t=',
	'ACP_CATTRUM_NOTE_FOUND_USERNAME'			=> 'NOTE: Username is a string and can be found in General > Manage users',
	'ACP_CATTRUM_NOTE_EMPTY_FORUM_LIST'			=> 'NOTE: Select no forum will result of the block deletion',
	
	'LOG_ACP_CATTRUM_SETTINGS'				=> '<strong>cattrum settings updated</strong>',

	'AUTH_PROVIDER_OAUTH_SERVICE_CATTRUM_DISCORD'	=> 'Discord',
]);
