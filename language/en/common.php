<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, [

	'AUTH_PROVIDER_OAUTH_SERVICE_CATTRUM_DISCORD'	=> 'Discord',
	'AUTH_PROVIDER_OAUTH_SERVICE_CATTRUM_TWITCH'	=> 'Twitch',

	'CATTRUM_EXCEPTION_TOKEN'				=> 'Something went wrong requesting a OAuth2 access token.<br>
														Original error message:<br>
														<samp class="error">%s</samp><br><br>
														<em>Did you perhaps refresh the page after linking an account?</em>',

	'CATTRUM_EXCEPTION_USER_INFO'			=> 'Something went wrong requesting the OAuth2 account information.<br><br>
														Original error message:<br>
														<samp class="error">%s</samp>',

	'USERNAME_ALREADY_PRESENT'				=> '<samp class="error">Something went wrong, the username %s is already register. </samp><br><br>
													You can link the external account to the already existing forum account by filling the username and password fields during the register process.<br><br>
													<em>You can also use the "I forgot my password" function to retrieve the forum account.</em>',

	'CATTRUM_DISCORD_NEW_POST'				=> '📩*** New message in topic: ***%s',
	'CATTRUM_DISCORD_UPDATE_POST'			=> '📩✎*** Update message in topic: ***%s',
	'CATTRUM_DISCORD_NEW_TOPIC'				=> '✉*** New topic: ***%s',
	'CATTRUM_DISCORD_UPDATE_TOPIC'			=> '✉✎*** Update topic: ***%s',

]);
