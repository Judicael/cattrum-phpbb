<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, [

	'AUTH_PROVIDER_OAUTH_SERVICE_CATTRUM_DISCORD'	=> 'Discord',
	'AUTH_PROVIDER_OAUTH_SERVICE_CATTRUM_TWITCH'	=> 'Twitch',

	'CATTRUM_EXCEPTION_TOKEN'				=> 'Un problème est survenu pour obtenir le jeton d\'accès OAuth2.<br>
														Message d\'erreur originel :<br>
														<samp class="error">%s</samp><br><br>
														<em>Did you perhaps refresh the page after linking an account?</em>',

	'CATTRUM_EXCEPTION_USER_INFO'			=> 'Un problème est survenu pour obtenir les informations de compte OAuth2.<br><br>
														Message d\'erreur originel :<br>
														<samp class="error">%s</samp>',

	'USERNAME_ALREADY_PRESENT'				=> '<samp class="error">Un problème est survenu, le nom d\'utilisateur %s existe déjà. </samp><br><br>
													Vous pouvez lier le compte externe au compte forum déjà existant en remplissant les champs nom d\' utilisateur et mot de passe lors de la procédure d\'enregistrement.<br><br>
													<em>Vous pouvez aussi utiliser la fonction "J\'ai oublié mon mot depasse" pour retrouver le compte forum.</em>',

	'CATTRUM_DISCORD_NEW_POST'				=> '📩*** Nouveau message pour le sujet : ***%s',
	'CATTRUM_DISCORD_UPDATE_POST'			=> '📩✎*** Message modifié pour le sujet : ***%s',
	'CATTRUM_DISCORD_NEW_TOPIC'				=> '✉*** Nouveau sujet : ***%s',
	'CATTRUM_DISCORD_UPDATE_TOPIC'			=> '✉✎*** Modification du sujet : ***%s',
]);
