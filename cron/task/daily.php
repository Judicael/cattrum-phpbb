<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\cron\task;

/**
 * @ignore
 */
use gaki\cattrum\service\db_service; 

/**
 * cattrum cron task.
 */
class daily extends \phpbb\cron\task\base
{
	/**
	 * How often we run the cron (in seconds).
	 * @var int
	 */
	protected $cron_frequency = 86400;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var \gaki\cattrum\service\db_service */
	protected $db_service;		

	/**
	 * Constructor
	 *
	 * @param \phpbb\config\config $config Config object
	 */
	public function __construct(
		\phpbb\config\config $config,
		db_service $db_service)
	{
		$this->config = $config;
		$this->db_service	= $db_service;
	}

	/**
	 * Runs this cron task.
	 *
	 * @return void
	 */
	public function run()
	{
		// Delete all temp data which are more than one day old
		$previousDayTime = time() - (1 * 24 * 60 * 60);
		$this->db_service->delete_values(db_service::TYPE_AUTH_TEMP, null, $previousDayTime, $previousDayTime);

		// Delete all data which are more than 183 day old
		$previousDayTime = time() - (183 * 24 * 60 * 60);
		$this->db_service->delete_values(db_service::TYPE_CANDIDATE_CONTENT, null, $previousDayTime, $previousDayTime);
		$this->db_service->delete_values(db_service::TYPE_CANDIDATE_TOPIC, null, $previousDayTime, $previousDayTime);

		// Update the cron task run time here if it hasn't
		// already been done by your cron actions.
		$this->config->set('cattrum_cron_last_run', time(), false);
	}

	/**
	 * Returns whether this cron task can run, given current board configuration.
	 *
	 * For example, a cron task that prunes forums can only run when
	 * forum pruning is enabled.
	 *
	 * @return bool
	 */
	public function is_runnable()
	{
		return true;
	}

	/**
	 * Returns whether this cron task should run now, because enough time
	 * has passed since it was last run.
	 *
	 * @return bool
	 */
	public function should_run()
	{
		return $this->config['cattrum_cron_last_run'] < time() - $this->cron_frequency;
	}
}
