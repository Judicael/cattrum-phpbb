<?php
/**
 *
 * DOL - Discord OAuth2 light. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2019, phpBB Studio, https://www.phpbbstudio.com
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\auth;

/**
 * @ignore
 */
use gaki\cattrum\service\db_service; 
use phpbb\auth\provider\oauth\service\exception;
use OAuth\Common\Http\Exception\TokenResponseException;
use OAuth\Common\Exception\Exception as AccountResponseException;

/**
* The provider name is: CATTRUM_DISCORD
* Need to allow Discord callback url (for login and link button in user profile):
* http://localhost/phpBB3/ucp.php?mode=login&login=external&oauth_service=cattrum_twitch
* http://localhost/phpBB3/ucp.php?i=ucp_auth_link&mode=auth_link&link=1&oauth_service=cattrum_twitch
* it3ri6x0yz7w6gd6t7lomz6u8suo87
*/
class twitch extends \phpbb\auth\provider\oauth\service\base
{
	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\language\language */
	protected $lang;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var \gaki\cattrum\service\db_service */
	protected $db_service;		

	/**
	 * Constructor.
	 *
	 * @param \phpbb\config\config				$config		Config object
	 * @param \phpbb\language\language			$lang		Language object
	 * @param \phpbb\request\request_interface	$request	Request object
	 * @access public
	 */
	public function __construct(
		\phpbb\config\config $config,
		\phpbb\language\language $lang,
		\phpbb\request\request_interface $request,
		db_service $db_service
	)
	{
		$this->config		= $config;
		$this->lang			= $lang;
		$this->request		= $request;
		$this->db_service	= $db_service;

		// Register a new provider for Lusitanian PHPoAuthLib
		include_once('Cattrum_twitch.php');
	}

	/**
	 * {@inheritdoc}
	 */
	public function get_service_credentials()
	{
		return [
			'key'		=> $this->config['auth_oauth_cattrum_twitch_key'],
			'secret'	=> $this->config['auth_oauth_cattrum_twitch_secret'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function perform_auth_login()
	{
		return $this->perform(true);
	}

	/**
	 * {@inheritdoc}
	 */
	public function perform_token_auth()
	{
		return $this->perform(false);
	}

	private function perform($requestNewToken) { 
		if (!($this->service_provider instanceof \OAuth\OAuth2\Service\cattrum_twitch))
		{
			throw new exception('AUTH_PROVIDER_OAUTH_ERROR_INVALID_SERVICE_TYPE');
		}

		if ($requestNewToken) {
			/* This was a callback request from Discord, get the token */
			try
			{
				$this->service_provider->requestAccessToken($this->request->variable('code', ''));
			}
			catch (TokenResponseException $e)
			{
				trigger_error($this->lang->lang('CATTRUM_EXCEPTION_TOKEN', $e->getMessage()), E_USER_WARNING);
			}
		} 

		$result['id'] = '';

		/* Send a request with it */
		try
		{
			$clientId = $this->get_service_credentials()['key'];
			$result = json_decode($this->service_provider->request('/users', 'GET', null, array('Client-Id' => $clientId)), true);
			$result = $result['data'][0]; // Always only one result (the token is issued for only one user)
			// Sample of result:
			// {"data":[
			//	{"id":"420000042",
			//	 "login":"wonderfulLogin",
			//   "display_name":"wonderfullDisplayname",
			//   "type":"",
			//   "broadcaster_type":"",
			//   "description":"",
			//   "profile_image_url":"https://static-cdn.jtvnw.net/jtv_user_pictures/cdddddde-9c0a-42da-9ac5-3ac428ca0610-profile_image-300x300.png",
			//   "offline_image_url":"",
			//   "view_count":0,
			//   "email":"wonderfulLogin@mail.com",
			//   "created_at":"2020-12-03T16:30:29.043781Z"
			// }]}
		}
		catch (AccountResponseException $e)
		{
			trigger_error($this->lang->lang('CATTRUM_EXCEPTION_USER_INFO', $e->getMessage()), E_USER_WARNING);
		}

		// Note: avatar need to finish with .png
		$user_info = array(
			'id'       => $result['id'],
			'username' => $result['display_name'],
			'avatar'   => str_replace('300x300', '150x150', $result['profile_image_url']),
			'email'    => $result['email'],
			'verified' => true,
		);
		$this->db_service->set_or_update_values(db_service::TYPE_AUTH_TEMP, $this->db_service->get_session_id(), $user_info);		

		// We prefix by the provider name in case of 2 provider have the same id value 
		// This will simplify the cattrum_values table 
		return 'cattrum_twitch::' . $result['id'];
	}	
}
