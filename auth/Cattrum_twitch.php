<?php
/**
 *
 * This class have beean instired by
 *
 * @copyright (c) 2019, phpBB Studio, https://www.phpbbstudio.com
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace OAuth\OAuth2\Service;

use OAuth\OAuth2\Token\StdOAuth2Token;
use OAuth\Common\Http\Exception\TokenResponseException;
use OAuth\Common\Http\Uri\Uri;
use OAuth\Common\Consumer\CredentialsInterface;
use OAuth\Common\Http\Client\ClientInterface;
use OAuth\Common\Http\Client\CurlClient;
use OAuth\Common\Storage\TokenStorageInterface;
use OAuth\Common\Http\Uri\UriInterface;

class Cattrum_twitch extends AbstractService
{
	/**
	 * Returns the user object of the requester's account.
	 * (For OAuth2, this requires the identify scope, which will return the object without an email)
	 *
	 * @see https://dev.twitch.tv/docs/api/reference#get-users
	 */
	const SCOPE_IDENTIFY = 'user:read:email';

	/**
	 * twitch constructor.
	 *
	 * @param \OAuth\Common\Consumer\CredentialsInterface	$credentials
	 * @param \OAuth\Common\Http\Client\ClientInterface		$httpClient
	 * @param \OAuth\Common\Storage\TokenStorageInterface	$storage
	 * @param array											$scopes
	 * @param \OAuth\Common\Http\Uri\UriInterface|null		$baseApiUri
	 */
	public function __construct(
		CredentialsInterface $credentials,
		ClientInterface $httpClient,
		TokenStorageInterface $storage,
		$scopes = [],
		UriInterface $baseApiUri = null
	)
	{
		if (empty($scopes))
		{
			/**
			 * You can specify multiple scopes by separating them with a space
			 * (implode the array with a space separator). See below.
			 */
			$scopes = [self::SCOPE_IDENTIFY];
		}

		// Use a not deprecated HTTP client (need php-curl extension)
		$httpClient = new CurlClient();
		parent::__construct($credentials, $httpClient, $storage, $scopes, $baseApiUri);

		if (null === $baseApiUri)
		{
			/**
			 * Omitting the version number from the route will route
			 * requests to the current default version. (v5 as of 2020-12-09)
			 *
			 * @see https://dev.twitch.tv/docs/api
			 */
			$this->baseApiUri = new Uri('https://api.twitch.tv/helix/');
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAuthorizationEndpoint()
	{
		return new Uri('https://id.twitch.tv/oauth2/authorize');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAccessTokenEndpoint()
	{
		return new Uri('https://id.twitch.tv/oauth2/token');
	}

	/**
	 * {@inheritdoc}
	 */
	protected function getAuthorizationMethod()
	{
		return static::AUTHORIZATION_METHOD_HEADER_BEARER;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function parseAccessTokenResponse($responseBody)
	{
		$data = json_decode($responseBody, true);

		if (null === $data || !is_array($data))
		{
			throw new TokenResponseException('Unable to parse response.');
		}
		else if (isset($data['error']))
		{
			/* We are not sure if "$data['error']" might be an array */
			$message = is_array($data['error']) ? implode('<br>', $data['error']) : $data['error'];

			throw new TokenResponseException('Error in retrieving token: "' . $message . '"');
		}

		/**
		 * Twitch's token expires in
		 * Let the logic discover it itself though.
		 */
		$token = new StdOAuth2Token();

		$token->setAccessToken($data['access_token']);

		if (isset($data['expires_in']))
		{
			$token->setLifetime($data['expires_in']);

			unset($data['expires_in']);
		}

		if (isset($data['refresh_token']))
		{
			$token->setRefreshToken($data['refresh_token']);

			unset($data['refresh_token']);
		}

		unset($data['access_token']);

		$token->setExtraParams($data);

		return $token;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAuthorizationUri(array $additionalParameters = [])
	{
		$parameters = array_merge(
			$additionalParameters,
			[
				'client_id'			=> $this->credentials->getConsumerId(),

				/** @see https://dev.twitch.tv/docs/authentication/getting-tokens-oauth/#oauth-authorization-code-flow */
				'grant_type'		=> 'authorization_code',
				'response_type'		=> 'code',
				'redirect_uri'		=> $this->credentials->getCallbackUrl(),
			]
		);

		/**
		 * Scope is a list of OAuth2 scopes separated by url encoded spaces.
		 * Here the url will be encoded later by the logic, so use a normal space.
		 */
		$parameters['scope'] = implode(' ', $this->scopes);

		/**
		 * If the user has previously authorized our application
		 * then skip the authorization screen and redirect it back to us.
		 */
		$parameters['prompt'] = 'none';

		/**
		 * Prevent CSRF and Clickjacking.
		 * That's optionaly requested by Twitch
		 */
		$parameters['state'] = $this->generateAuthorizationState();

		/* Store the generated state */
		$this->storeAuthorizationState($parameters['state']);

		/* Build the url */
		$url = clone $this->getAuthorizationEndpoint();

		foreach ($parameters as $key => $val)
		{
			$url->addToQuery($key, $val);
		}

		return $url;
	}
}
