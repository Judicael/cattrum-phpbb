<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\acp;

/**
 * cattrum ACP module info.
 */
class main_info
{
	public function module()
	{
		return [
			'filename'	=> '\gaki\cattrum\acp\main_module',
			'title'		=> 'ACP_CATTRUM_TITLE',
			'modes'		=> [
				'settings'	=> [
					'title'	=> 'ACP_CATTRUM',
					'auth'	=> 'ext_gaki/cattrum && acl_a_board',
					'cat'	=> ['ACP_CATTRUM_TITLE'],
				],
			],
		];
	}
}
