<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\event;

/**
 * @ignore
 */
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use gaki\cattrum\service\db_service;
use gaki\cattrum\service\user_service;
use gaki\cattrum\service\webhook_service;

/**
 * cattrum Event listener.
 */
class main_listener implements EventSubscriberInterface
{
	
	public static function getSubscribedEvents()
	{
		return [
			'core.page_header_after'                    => 'template_setup',
			'core.user_setup'							=> 'load_language_on_setup',
			'core.ucp_register_modify_template_data'	=> 'register_modify_template_data',
			'core.submit_post_end'						=> 'submit_post_end',
		];
	}

	/* @var \phpbb\language\language */
	protected $language;

    // @var \phpbb\template\template
	protected $template;
	
    // @var \phpbb\config\config
	protected $config;	
	
    // @var \phpbb\controller\helper
    protected $controller_helper;	

	/** @var \gaki\cattrum\service\db_service  */
	protected $db_service;

	/** @var \gaki\cattrum\service\user_service  */
	protected $user_service;

	/** @var \gaki\cattrum\service\webhook_service  */
	protected $webhook_service;		

    /** @var string */
    protected $php_ext;	

	/**
	 * Constructor
	 */
	public function __construct(
		\phpbb\language\language $language, 
		\phpbb\template\template $template,
		\phpbb\config\config $config,
		\phpbb\controller\helper $controller_helper,
		db_service $db_service,
		user_service $user_service,
		webhook_service $webhook_service,
		$php_ext)
	{
		$this->language				= $language;
		$this->template				= $template;
		$this->config				= $config;
		$this->controller_helper	= $controller_helper;
		$this->db_service			= $db_service;
		$this->user_service			= $user_service;
		$this->webhook_service		= $webhook_service;
		$this->php_ext				= $php_ext;
		// Can also have request and user for exemple
	}

	/**
	 * Load common language files during user setup
	 *
	 * @param \phpbb\event\data	$event	Event object
	 */
	public function load_language_on_setup($event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = [
			'ext_name' => 'gaki/cattrum',
			'lang_set' => 'common',
		];
		$event['lang_set_ext'] = $lang_set_ext;
	}

    /**
     * Initialise some global template var for cattrum.
     */
    public function template_setup($event)
    {
		// Flag if we need to add nav bar buttons
		if (!empty($this->config['gaki_cattrum_add_navbar'])) { 
			$this->template->assign_vars(array(
				'GAKI_CATTRUM_ADD_NAVBAR' => 1
			));
		}

		// Flag about discord enable
		if (!empty($this->config['auth_oauth_cattrum_discord_key'])) {
			$this->template->assign_vars(array(
				'GAKI_CATTRUM_DISCORD' => 1
			));
		}

		// Flag about twitch enable
		if (!empty($this->config['auth_oauth_cattrum_twitch_key'])) {
			$this->template->assign_vars(array(
				'GAKI_CATTRUM_TWITCH' => 1
			));
		}		
	}		

	/**
	 * Intercept the register button clic for do special treatment if in cattrum auth context.
	 * Use for register a new user
	 * 
	 * @event  core.ucp_register_modify_template_data
	 * @param \phpbb\event\data $event The event object
	 * @return void
	 * @access public
	 */
	public function register_modify_template_data($event)
	{
		//$row
		$providerPrefix = 'cattrum_';
		$providerName = $event['s_hidden_fields']['login_link_oauth_service'];
		if(substr($providerName, 0, strlen($providerPrefix)) === $providerPrefix
			&& $event['s_hidden_fields']['login_link_link_method']==='login_link'
			&& $event['s_hidden_fields']['agreed']==='true') {
				// The user have clic on REGISTER and aggreed the terms form
				
				// Create the user
				$user_infos = $this->db_service->get_values(db_service::TYPE_AUTH_TEMP, $this->db_service->get_session_id());
				if ($user_infos !== null && count($user_infos) === 1 ) {
					$this->user_service->create_user($user_infos[0][db_service::TABLE_VALUES_VALUE], $providerName);
					// Now that the user is created we relog it
					// http://localhost/phpBB3/ucp.php?mode=login&login=external&oauth_service=$providerName
					$url = $this->phpbb_root_path . 'ucp.' . $this->php_ext . '?mode=login&login=external&oauth_service=' . $providerName;
					$url = append_sid($url);
					redirect($url, false, false);
				}
				
				trigger_error('ERROR: Cannot create the new account automaticaly.');			
		} 
	}	

	public function submit_post_end($event) {
		// Check for visibility of the post/topic. We don't send notifications for content that are hidden from normal users.
		// Note that there are three visibility settings here. The first is the post visibility when it is generated. For example,
		// users may require moderator approval before their posts appear. The other two are the existing visibility status of the topic and post.
		if ($event['post_visibility'] === 0 || $event['data']['topic_visibility'] === 0 || $event['data']['post_visibility'] === 0) {
			return;
		}

		// Verify that the forum that the post submit action happened in has notifications enabled. If not, we have nothing further to do.
		$whList = $this->db_service->get_values(db_service::TYPE_DISCORD_NOTIFICATION_FORUM, $event['data']['forum_id']);
		if (count($whList) === 0) {
			return;
		}

		// Build an array of the event data that we may need to pass along to the function that will construct the notification message
		$post_data = array(
			'user_id'			=> (int) $event['data']['poster_id'],
			'user'				=> $this->db_service->query_user($event['data']['poster_id']),
			'forum_id'			=> $event['data']['forum_id'],
			'forum_name'		=> $event['data']['forum_name'],
			'topic_id'			=> $event['data']['topic_id'],
			'topic_title'		=> $event['data']['topic_title'],
			'post_id'			=> $event['data']['post_id'],
			'post_title'		=> $event['subject'],
			'edit_user_id'		=> (int) $event['data']['post_edit_user'],
			'edit_user'			=> $this->language->lang('UNKNOWN_USER'),
			'edit_reason'		=> $event['data']['post_edit_reason'],
			'content'			=> $event['data']['message'],
			// Additionnal data for transform the data base  storage of BBCODE to HTML
			'bbcode_uid'		=> $event['data']['bbcode_uid'],
			'bbcode_bitfield' 	=> $event['data']['bbcode_bitfield'],
			// URL algo come from \phpbb\notification\type\post 
			'url_post'			=> generate_board_url() . '/viewtopic.' . $this->php_ext . '?p=' . $event['data']['post_id'] . '#p' . $event['data']['post_id'],
			'url_topic'			=> generate_board_url() . '/viewtopic.' . $this->php_ext . '?f=' . $event['data']['forum_id'] . '&t=' . $event['data']['topic_id'],
		);

		// If anonymous user we use the username of the event
		if ($post_data['user'] === null || $post_data['user_id'] < ANONYMOUS) { 
			$post_data['user'] = array (
				'username' => $event['username']
			);
		} 

		// Get user which edit
		if ($post_data['edit_user_id'] == $post_data['user_id']) {
			$post_data['edit_user'] = $post_data['user'];
		} else {
			$post_data['edit_user'] = $this->language->lang('UNKNOWN_USER');
			$edit_user = $this->db_service->query_user($post_data['edit_user_id']);
			if ($edit_user != null)
			{
				$post_data['edit_user'] = $edit_user;
			}
		}

		// If anonymous edit user we use the username of the event
		if ($post_data['edit_user'] === null || $post_data['edit_user_id'] < ANONYMOUS) { 
			$post_data['edit_user'] = array (
				'username' => $event['username']
			);
		} 	

		// Get full URL for avatar
		if ($post_data['user']['avatar']) {
			$post_data['user']['avatar'] = generate_board_url() . '/download/file.' . $this->php_ext . '?avatar=' . $post_data['user']['avatar'];
		} else {
			$post_data['user']['avatar'] = 'https://robohash.org/'. $post_data['user']['username'] .'.png?size=60x60&set=set4&regex-fake.png';
		}  
		if ($post_data['edit_user']['avatar']) {
			$post_data['edit_user']['avatar'] = generate_board_url() . '/download/file.' . $this->php_ext . '?avatar=' . $post_data['edit_user']['avatar'];
		} else {
			$post_data['edit_user']['avatar'] = 'https://robohash.org/'. $post_data['edit_user']['username'] .'.png?size=60x60&set=set4&regex-fake.png';
		}  

		// Compute if it's a new/edition of a topic or post
		$isNewTopic  = $event['mode'] == 'post';
		$isNewPost   = $event['mode'] == 'reply' || $event['mode'] == 'quote';
		$isEdit      = $event['mode'] == 'edit' || $event['mode'] == 'edit_topic' || $event['mode'] == 'edit_first_post' || $event['mode'] == 'edit_last_post';
		// If the post that was edited is the first one in the topic, we consider this a topic update event.
		$isEditTopic = $isEdit && $event['data']['post_id'] == $event['data']['topic_first_post_id'];
		$isEditPost  = $isEdit && !$isEditTopic;		

		// Loop on all web hook for find one matching
		foreach ($whList as $wh) {
			$value = $wh[db_service::TABLE_VALUES_VALUE]; 

			// Get allowed list of user and topics (empty array il all allowed)
			$allowedTopicIds  = preg_split('/[, ;\n\r]+/', $value['topics'], -1, PREG_SPLIT_NO_EMPTY);
			$allowedUsernames = preg_split('/[, ;\n\r]+/', $value['users'], -1, PREG_SPLIT_NO_EMPTY);
			$isAllowedTopic        = empty($allowedTopicIds)  || in_array($post_data['topic_id'], $allowedTopicIds);
			$isAllowedUsername     = empty($allowedUsernames) || in_array($post_data['user']['username'] , $allowedUsernames);
			$isAllowedEditUsername = empty($allowedUsernames) || in_array($post_data['edit_user']['username'] , $allowedUsernames);

			if ($isNewTopic && $value['new_topic'] && $isAllowedTopic && $isAllowedUsername) {
				// New topic
				$this->webhook_service->newTopic($value['urls'], $post_data);
			} elseif ($isNewPost && $value['new_post'] && $isAllowedTopic && $isAllowedUsername) {
				// New post
				$this->webhook_service->newPost($value['urls'], $post_data);
			} elseif ($isEditTopic && $value['update_topic'] && $isAllowedTopic && $isAllowedEditUsername) {
				// Update topic
				$this->webhook_service->updateTopic($value['urls'], $post_data);
			} elseif ($isEditPost && $value['update_post'] && $isAllowedTopic && $isAllowedEditUsername) {
				// Update post
				$this->webhook_service->updatePost($value['urls'], $post_data);
			}
		}
	} 
}
