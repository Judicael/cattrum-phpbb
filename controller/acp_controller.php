<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\controller;

/**
 * @ignore
 */
use gaki\cattrum\service\db_service; 

/**
 * cattrum ACP controller.
 */
class acp_controller
{
	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\language\language */
	protected $language;

	/** @var \phpbb\log\log */
	protected $log;

	/** @var \phpbb\request\request */
	protected $request;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\user */
	protected $user;

	/** @var string Custom form action */
	protected $u_action;

	/** @var \gaki\cattrum\service\db_service */
	protected $db_service;	

	/**
	 * Constructor.
	 *
	 * @param \phpbb\config\config		$config		Config object
	 * @param \phpbb\language\language	$language	Language object
	 * @param \phpbb\log\log			$log		Log object
	 * @param \phpbb\request\request	$request	Request object
	 * @param \phpbb\template\template	$template	Template object
	 * @param \phpbb\user				$user		User object
	 */
	public function __construct(
		\phpbb\config\config $config, 
		\phpbb\language\language $language, 
		\phpbb\log\log $log, 
		\phpbb\request\request $request, 
		\phpbb\template\template $template, 
		\phpbb\user $user,
		db_service $db_service)
	{
		$this->config		= $config;
		$this->language		= $language;
		$this->log			= $log;
		$this->request		= $request;
		$this->template		= $template;
		$this->user			= $user;
		$this->db_service	= $db_service;
	}

	/**
	 * Display the options a user can configure for this extension.
	 *
	 * @return void
	 */
	public function display_options()
	{
		// Add our common language file
		$this->language->add_lang('common', 'gaki/cattrum');

		// Create a form key for preventing CSRF attacks
		add_form_key('gaki_cattrum_acp');

		// Create an array to collect errors that will be output to the user
		$errors = [];

		// Is the form being submitted to us?
		if ($this->request->is_set_post('submit')) {
			// Test if the submitted form is valid
			if (!check_form_key('gaki_cattrum_acp')) {
				$errors[] = $this->language->lang('FORM_INVALID');
			}

			// If no errors, process the form data
			if (empty($errors)) {	
				// Save GENERAL part	
				if ('set_settings' === $this->request->variable('sub_action', '')) {
					// Set the options the user configured
					$this->config->set('gaki_cattrum_add_navbar', $this->request->variable('gaki_cattrum_add_navbar', 1));
					$this->config->set('gaki_cattrum_discord_bot_allowed', $this->request->variable('gaki_cattrum_discord_bot_allowed', ''));
					$this->config->set('gaki_cattrum_discord_bot_forum', $this->request->variable('gaki_cattrum_discord_bot_forum', 0));
					$this->db_service->set_or_update_values(db_service::TYPE_CANDIDATE_TEMPLATE, 0, $this->request->variable('gaki_cattrum_candidate_template', ''));


					// Add option settings change action to the admin log
					$this->log->add('admin', $this->user->data['user_id'], $this->user->ip, 'LOG_ACP_CATTRUM_SETTINGS');
	
					// Option settings have been updated and logged
					// Confirm this to the user and provide link back to previous page
					trigger_error($this->language->lang('ACP_CATTRUM_SETTING_SAVED') . adm_back_link($this->u_action));
				}

				// Save Discord Webhooks part
				if ('set_webhook' === $this->request->variable('sub_action', '')) {
					// We buil a array in which each line is an entry into database
					$formWhList = $this->request->raw_variable('wh_list', array(), false, \phpbb\request\request_interface::POST);
					$dbRows = $this->convertFormToDbRows($formWhList);

					// Delete all DB entries
					$this->db_service->delete_values(db_service::TYPE_DISCORD_NOTIFICATION_FORUM);
					foreach ($dbRows as $row) {
						$this->db_service->set_values(db_service::TYPE_DISCORD_NOTIFICATION_FORUM, $row['forumId'], $row['value']);
					}

					// Option settings have been updated and logged
					// Confirm this to the user and provide link back to previous page
					trigger_error($this->language->lang('ACP_CATTRUM_SETTING_SAVED') . adm_back_link($this->u_action));
				} 
			}
		} elseif ($this->request->is_set_post('add_wh')) {
			// Add an empty WH block
			$this->db_service->set_values(db_service::TYPE_DISCORD_NOTIFICATION_FORUM, 0, array(
				'block_id' => uniqid(),
				'urls' => 'Url of the Discord Webhook'));
		} elseif ($this->request->is_set_post('remove_wh')) {
			$removeBlockId = $this->request->variable('remove_wh', '');
			// We delete all DB entry which have this block_id
			$dbRows = $this->db_service->get_values(db_service::TYPE_DISCORD_NOTIFICATION_FORUM);
			foreach ($dbRows as $dbRow) {
				if ($removeBlockId  === $dbRow[db_service::TABLE_VALUES_VALUE]['block_id']) {
					$this->db_service->delete_values(db_service::TYPE_DISCORD_NOTIFICATION_FORUM, null, null, null, null, $dbRow[db_service::TABLE_VALUES_ID]);
				} 
			}

		}

		$s_errors = !empty($errors);

		// Read db value
		$dbRows = $this->db_service->get_values(db_service::TYPE_DISCORD_NOTIFICATION_FORUM);
		$formWhList = $this->convertDbRowsToForm($dbRows);

		// Set output variables for display in the template
		$gaki_cattrum_candidate_template = null;
		$dbRows = $this->db_service->get_values(db_service::TYPE_CANDIDATE_TEMPLATE, 0);
		if (!empty($dbRows)) {
			$gaki_cattrum_candidate_template = $dbRows[0][db_service::TABLE_VALUES_VALUE]; 
		} 
		$this->template->assign_vars([
			'S_ERROR'		=> $s_errors,
			'ERROR_MSG'		=> $s_errors ? implode('<br />', $errors) : '',

			'U_ACTION'		=> $this->u_action,

			'GAKI_CATTRUM_ADD_NAVBAR'			=> (bool) $this->config['gaki_cattrum_add_navbar'],
			'GAKI_CATTRUM_DISCORD_BOT_ALLOWED'	=> (string) $this->config['gaki_cattrum_discord_bot_allowed'],
			'GAKI_CATTRUM_DISCORD_BOT_FORUM'	=> make_forum_select((int) $this->config['gaki_cattrum_discord_bot_forum'], false, true, true),
			'GAKI_CATTRUM_CANDIDATE_TEMPLATE'	=> $gaki_cattrum_candidate_template
		]);

		$this->template->assign_block_vars_array('wh_list', $formWhList);
	}

	private function convertFormToDbRows($formWhList) {
		$dbRows = array();
		foreach ($formWhList as $wh) {
			// Reorganize by forum id
			foreach ($wh['wh_forum'] as $forumId) {
				$dbRows[] = array(
					'forumId' => $forumId,
					'value' => array(
						'block_id'		=> $wh['wh_block_id'], // Use to keep track of same UI block origin
						'urls'			=> $wh['wh_urls'],
						'new_topic'		=> $wh['wh_new_topic'],
						'new_post'		=> $wh['wh_new_post'],
						'update_topic'	=> $wh['wh_update_topic'],
						'update_post'	=> $wh['wh_update_post'],
						'topics'		=> $wh['wh_topics'],
						'users'			=> $wh['wh_users'],
					),
				);
			}
		}
		return $dbRows;
	} 

	private function convertDbRowsToForm($dbRows) {
		$formWhList = array();
		foreach ($dbRows as $row) {
			$value = $row[db_service::TABLE_VALUES_VALUE];
			if (!array_key_exists($value['block_id'], $formWhList)) {
				$formWhList[$value['block_id']] = array();
			}
			$formWhList[$value['block_id']]['WH_BLOCK_ID'] = $value['block_id'];
			$formWhList[$value['block_id']]['WH_FORUM_OPTIONS_IDS'][] = $row[db_service::TABLE_VALUES_KEY];
			$formWhList[$value['block_id']]['WH_NEW_TOPIC'] = $value['new_topic'];
			$formWhList[$value['block_id']]['WH_NEW_POST'] = $value['new_post'];
			$formWhList[$value['block_id']]['WH_UPDATE_TOPIC'] = $value['update_topic'];
			$formWhList[$value['block_id']]['WH_UPDATE_POST'] = $value['update_post'];
			$formWhList[$value['block_id']]['WH_URLS'] = $value['urls'];
			$formWhList[$value['block_id']]['WH_TOPICS'] = $value['topics'];
			$formWhList[$value['block_id']]['WH_USERS'] = $value['users'];

		}
		// Get the list of forum for disply it and select the correct one
		foreach ($formWhList as $block_id => $value) {
			$formWhList[$block_id]['WH_FORUM_OPTIONS'] =  make_forum_select($value['WH_FORUM_OPTIONS_IDS'], false, true, true);
		}
		return $formWhList;
	} 	

	/**
	 * Set custom form action.
	 *
	 * @param string	$u_action	Custom form action
	 * @return void
	 */
	public function set_page_url($u_action)
	{
		$this->u_action = $u_action;
	}
}
