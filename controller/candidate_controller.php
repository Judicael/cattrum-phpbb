<?php
/**
 *
 * dazdaz. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, gaki
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\controller;

/**
 * @ignore
 */
use Symfony\Component\HttpFoundation\JsonResponse;
use OAuth\Common\Http\Client\CurlClient;
use OAuth\Common\Http\Uri\Uri; 
use gaki\cattrum\service\db_service;
use gaki\cattrum\service\user_service;

/**
 * main controller.
 */
// JS Axios for post form data
// const FormData = require('form-data');
// const fs = require('fs');
//
// const axios = require('axios');
// let form = new FormData();
// form.append('file', fs.createReadStream(__dirname + '/README.md'), {
// filename: '111.md'
// });
// axios.create({
// headers: form.getHeaders()
// }).post('http://localhost:8001/sample/upload', form).then(response => {
// console.log(response);
// }).catch(error => {
// if (error.response) {
// console.log(error.response);
// }
// console.log(error.message);
// });
class candidate_controller
{
	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\controller\helper */
	protected $helper;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\language\language */
	protected $language;

	/** @var \phpbb\request\request */
	protected $request;

	/** @var \phpbb\user */
	protected $user;	

	/** @var \gaki\cattrum\service\db_service */
	protected $db_service;

	/** @var \gaki\cattrum\service\user_service */
	protected $user_service;	
	
    /** @var string */
	protected $phpbb_root_path;	
	
    /** @var string */
    protected $php_ext;	

	/**
	 * Constructor
	 *
	 * @param \phpbb\config\config		$config		Config object
	 * @param \phpbb\controller\helper	$helper		Controller helper object
	 * @param \phpbb\template\template	$template	Template object
	 * @param \phpbb\language\language	$language	Language object
	 */
	public function __construct(
		\phpbb\config\config $config, 
		\phpbb\controller\helper $helper, 
		\phpbb\template\template $template, 
		\phpbb\language\language $language,
		\phpbb\request\request $request,
		\phpbb\user $user,
		db_service $db_service,
		user_service $user_service,
		$phpbb_root_path,
		$php_ext)
	{
		$this->config			= $config;
		$this->helper			= $helper;
		$this->template			= $template;
		$this->language			= $language;
		$this->request			= $request;
		$this->user				= $user;
		$this->db_service		= $db_service;
		$this->user_service		= $user_service;
		$this->phpbb_root_path	= $phpbb_root_path;
		$this->php_ext			= $php_ext;		
	}

	/**
	 * Controller handler for route /demo/{name}
	 *
	 * @return \Symfony\Component\HttpFoundation\Response A Symfony Response object
	 */
	public function handle()
	{
		$this->checkAuth();

		$json_response = null;
		$code_response = 400;

		if ($this->request->is_set_post('mode')) {
			// Extract POST value: 
			//	{
			//		"username":"LeChat",
			//		"discordId": 458754321,
			//		"games":["Star Citizen"],
			//		"questions":{"100": "Question 01","150": "Question 02","200": "Question 03"},
			//		"answers":{"100":"Answers 01","150":"Answer 02 a", "200": "Answer 03"}
			//	}
			$value = json_decode($this->request->raw_variable('json', '{}'), true);
			$mode = $this->request->raw_variable('mode', '');
			$key = (int) $this->request->raw_variable('key', '');
			if (is_numeric($this->request->raw_variable('key', '')) == false) {
				throw new \phpbb\exception\http_exception(400, 'NOT_AUTHORISED', array(''));
			}
				
			switch ($mode) {
				case 'CANDIDATE_READ':
					$rows = $this->db_service->get_values(db_service::TYPE_CANDIDATE_CONTENT, $key);
					if (!empty($rows)) {
						$json_candidate = $rows[0][db_service::TABLE_VALUES_VALUE];
						$json_response = $json_candidate;
						$code_response = 200;
					} else {
						$json_response = null;
						$code_response = 404;
					}
					break;
				case 'CANDIDATE_WRITE':
					$rows = $this->db_service->get_values(db_service::TYPE_CANDIDATE_CONTENT, $key);
					// Merge already existing data with the new data
					if (!empty($rows) && $rows[0][db_service::TABLE_VALUES_VALUE] !== null) {
						$value = array_replace_recursive($rows[0][db_service::TABLE_VALUES_VALUE], $value);
					}
					// Save the new data
					$json_response = $this->db_service->set_or_update_values(db_service::TYPE_CANDIDATE_CONTENT, $key, $value);
					$code_response = 200;
					break;
				case 'CANDIDATE_SUBMIT':
					// Get if alreday posted (edition) or never posted (creation)
					$rows = $this->db_service->get_values(db_service::TYPE_CANDIDATE_TOPIC, $key);
					$topic_id = 0; // 0 for new topic
					if (!empty($rows) && array_key_exists('topic_id', $rows[0][db_service::TABLE_VALUES_VALUE])) {
						$topic_id = (int) $rows[0][db_service::TABLE_VALUES_VALUE]['topic_id'];
					}		
					// Get candidate data
					$rows = $this->db_service->get_values(db_service::TYPE_CANDIDATE_CONTENT, $key);
					// Post or update topic
					if (!empty($rows)) {
						$json_candidate = $rows[0][db_service::TABLE_VALUES_VALUE];

						// Get the post candidate template
						$gaki_cattrum_candidate_template = null;
						$dbRows = $this->db_service->get_values(db_service::TYPE_CANDIDATE_TEMPLATE, 0);
						if (!empty($dbRows)) {
							$gaki_cattrum_candidate_template = $dbRows[0][db_service::TABLE_VALUES_VALUE]; 
						} 

						// Set output variables for display in the template
						$this->template->assign_vars([
							'TEMPLATE'		=> $gaki_cattrum_candidate_template,
							'USERNAME'		=> $json_candidate['username'],
							'GAMES_STRING'	=> implode(', ',$json_candidate['games']),
							'GAMES'			=> $json_candidate['games'],
							'KEYS'			=> array_keys($json_candidate['questions']),
							'QUESTIONS'		=> $json_candidate['questions'],
							'ANSWERS'		=> $json_candidate['answers'],
						]);

						$content = $this->template->assign_display('cattrum_post_candidate.twig', '', true);

						// Create a message with submit_post()
						$forum_id = (int) $this->config['gaki_cattrum_discord_bot_forum'];
						$json_response  = $this->user_service->submit_post($json_candidate['username'], '[' . implode(', ',$json_candidate['games']) . '] ' . $json_candidate['username'], $content, $forum_id, $topic_id);
						$code_response = 200;
						$this->db_service->set_or_update_values(db_service::TYPE_CANDIDATE_TOPIC, $key, $json_response);
					}
					break;
				case 'CANDIDATE_DELETE':
					$this->db_service->delete_values(db_service::TYPE_CANDIDATE_CONTENT, $key);
					$json_response = $this->db_service->delete_values(db_service::TYPE_CANDIDATE_TOPIC, $key);
					$code_response = 200;
					break;									
				default:					
					throw new \phpbb\exception\http_exception(400, 'NO_ACTION', array(''));
					break;
			}
		}  
		

		// Return a JSON
		return new JsonResponse(
			array(
				'json'	=> $json_response,
			),
			$code_response			
		);
		// Return a web page
		//$l_message = !$this->config['gaki_cattrum_goodbye'] ? 'CATTRUM_HELLO' : 'CATTRUM_GOODBYE';
		//$this->template->assign_var('CATTRUM_MESSAGE', $this->language->lang($l_message, $name));
		//return $this->helper->render('@gaki_cattrum/cattrum_body.html', $name);				
	}

	/**
	 * Check the authentication. Fire an exception if not allowed.
	 */
	private function checkAuth() {
		// Check discord bot auth
		// HTTP Header 'Authorization Bot BOT_TOKEN.XXXX.XXXXXXXXXXXXXXX'
		// GET https://discord.com/api/oauth2/applications/@me 
		$token = $this->request->header('Cattrum-Authorization');		
		// @var \OAuth\Common\Http\Client\ClientInterface
		$httpClient = new CurlClient();
		//@var \OAuth\Common\Http\Uri\UriInterface 
		$uri = new Uri('https://discord.com/api/oauth2/applications/@me');
		try {
			$result = $httpClient->retrieveResponse($uri, null, array('Authorization' => 'Bot ' . $token), 'GET');
			$clientId = json_decode($result)->id;
			// Verify if the discord bot id is allowed
			$allowedClientIds = preg_split('/[, ;\n\r]+/', $this->config['gaki_cattrum_discord_bot_allowed']);
			if (!$allowedClientIds || !in_array($clientId, $allowedClientIds)) {
				throw new \phpbb\exception\http_exception(403, 'NO_AUTH_OPERATION', array($clientId));
			}
		} catch (\Exception $e) {
			throw new \phpbb\exception\http_exception(403, 'NO_AUTH_OPERATION', array($e->getMessage()));
		} 
	}

}
