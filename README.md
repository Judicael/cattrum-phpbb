# cattrum

## Installation

Copy the extension to phpBB/ext/gaki/cattrum

Go to "ACP" > "Customise" > "Extensions" and enable the "cattrum" extension.


## Configuration 

After you need to enable Oauth Authentication (it'll work in parralel of forum credentials).

Go to "ACP" > "General" > "Authentication" > "Select an authentication method:" > Oauth

And fill requiered information for discord and twitch provider.

Don't forget to allow redirect URI for discord:

https://your-forum.com/ucp.php?i=ucp_auth_link&mode=auth_link&link=1&oauth_service=cattrum_discord
https://your-forum.com/ucp.php?mode=login&login=external&oauth_service=cattrum_discord


Don't forget to allow redirect URI for twitch:

https://your-forum.com/ucp.php?i=ucp_auth_link&mode=auth_link&link=1&oauth_service=cattrum_twitch
https://your-forum.com/ucp.php?mode=login&login=external&oauth_service=cattrum_twitch


All other configuration is in:

"ACP" > "Extensions" > "Cattrum Settings"

NOTE: For avatar import, you may be need to increse the size of allowed avatar: 150x200 and 199680 Bytes. 

## License

[GNU General Public License v2](license.txt)
