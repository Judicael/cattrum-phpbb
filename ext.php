<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum;

/**
 * cattrum Extension base
 *
 * It is recommended to remove this file from
 * an extension if it is not going to be used.
 */
class ext extends \phpbb\extension\base
{

    /**
	 * Check whether the extension can be enabled.
	 * Provides meaningful(s) error message(s) and the back-link on failure.
	 * CLI compatible
	 *
	 * @return bool
	 */
	public function is_enableable()
	{
		$errorMessage = null;

		$user = $this->container->get('user');
		$user->add_lang_ext('gaki/cattrum', 'ext_require');

		if (!(phpbb_version_compare(PHPBB_VERSION, '3.2.7', '>=') && phpbb_version_compare(PHPBB_VERSION, '4.0.0@dev', '<')))
		{
			$errorMessage = $user->lang('EXTENSION_NOT_ENABLEABLE') . '<br><br>' . $user->lang('ERROR_PHPBB_VERSION', '3.2.7', '4.0.0@dev');
		}

		if (!function_exists('curl_exec')) {
			$errorMessage = $user->lang('EXTENSION_NOT_ENABLEABLE') . '<br><br>' . $user->lang('ERROR_PHP_CURL');
			$is_enableable = false;
		} 

		if (!class_exists('XSLTProcessor')) {
			$errorMessage = $user->lang('EXTENSION_NOT_ENABLEABLE') . '<br><br>' . $user->lang('ERROR_PHP_XSL');
		} 		

		return $errorMessage === null ? true : $errorMessage;
	}
}
