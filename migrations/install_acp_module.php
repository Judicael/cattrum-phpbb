<?php
/**
 *
 * cattrum. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2020, LeChat
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace gaki\cattrum\migrations;

class install_acp_module extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['gaki_cattrum_add_navbar']);
	}

	public static function depends_on()
	{
		return ['\phpbb\db\migration\data\v320\v320'];
	}

	public function update_data()
	{
		return [
			['config.add', ['gaki_cattrum_add_navbar', 1]],
			['config.add', ['gaki_cattrum_discord_bot_allowed', '']],
			['config.add', ['gaki_cattrum_discord_bot_forum', 0]],

			['module.add', [
				'acp',
				'ACP_CAT_DOT_MODS',
				'ACP_CATTRUM_TITLE'
			]],
			['module.add', [
				'acp',
				'ACP_CATTRUM_TITLE',
				[
					'module_basename'	=> '\gaki\cattrum\acp\main_module',
					'modes'				=> ['settings'],
				],
			]],
		];
	}
}
